package main

import (
    "fmt"
    "net/http"
)

func main() {
    http.HandleFunc("/", HelloServer)
    fmt.Print("Microservice 1 server listening on 0.0.0.0:8080")
    http.ListenAndServe(":8080", nil)
}

func HelloServer(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Microservice 1 says Hello, %s!", r.URL.Path[1:])
}
